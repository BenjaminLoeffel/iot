sudo usermod -aG docker telegraf
sudo systemctl stop telegraf
echo "please enter influxdb token:"
read influx_token
echo "entered token:"
echo $influx_token
sudo cp ./telegraf.conf /etc/telegraf/telegraf.conf
sudo sed -i "s/INFLUX_TOKEN/$influx_token/" /etc/telegraf/telegraf.conf
sudo systemctl start telegraf