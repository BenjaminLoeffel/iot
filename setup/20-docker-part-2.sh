curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker $USER
sudo apt-get install libffi-dev libssl-dev python3-dev python3 python3-pip -y
sudo pip3 install docker-compose
sudo cp ./daemon.json /etc/docker/
sudo reboot