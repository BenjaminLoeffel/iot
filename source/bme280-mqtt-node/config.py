import os


class Config:
    _mqtt_broker: str
    _mqtt_port: int

    def __init__(self, mqtt_broker: str, mqtt_port: int):
        self._mqtt_broker = mqtt_broker
        self._mqtt_port = mqtt_port

    @property
    def mqtt_broker(self) -> str:
        return self._mqtt_broker

    @property
    def mqtt_port(self) -> int:
        return self._mqtt_port


def config_by_name() -> Config:
    mqtt_broker = os.getenv("MQTT_BROKER") or "raspberrypi-argon.local"
    mqtt_port = os.getenv("MQTT_PORT") or 1883

    return Config(mqtt_broker=mqtt_broker,
                  mqtt_port=mqtt_port)
