from abc import ABC, abstractmethod

from adafruit_bme280 import basic as adafruit_bme280

from message_payload import IMessagePayload, MessagePayload


class IBme280Service(ABC):

    @abstractmethod
    def read(self) -> IMessagePayload:
        """Reads BME280 and returns IMessagePayload."""


class Bme280Service(IBme280Service):
    _bme280: adafruit_bme280.Adafruit_BME280

    def __init__(self, bme280: adafruit_bme280.Adafruit_BME280):
        self._bme280 = bme280

    def read(self) -> IMessagePayload:
        return MessagePayload(temperature=self._bme280.temperature,
                              humidity=self._bme280.relative_humidity,
                              pressure=self._bme280.pressure)
