import datetime
import time

import board
import paho.mqtt.client as mqtt
from adafruit_bme280 import basic as adafruit_bme280

from bme280_service import Bme280Service
from config import config_by_name
from message import Message
from message_metadata import MessageMetadata
from mqtt_service import MqttService

if __name__ == "__main__":
    config = config_by_name()

    i2c = board.I2C()
    bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c)
    bme280_service = Bme280Service(bme280)

    mqtt_client = mqtt.Client()
    mqtt_client.connect(host=config.mqtt_broker, port=config.mqtt_port)
    mqtt_service = MqttService(mqtt_client=mqtt_client)

    asset_guid = "123456789"
    asset_type_id = "abcd"

    while True:
        old_time = time.perf_counter()
        message_payload = bme280_service.read()
        message = Message(metadata=MessageMetadata(asset_guid=asset_guid,
                                                   asset_type_id=asset_type_id,
                                                   timestamp=datetime.datetime.now()),
                          payload=message_payload)
        mqtt_service.publish(topic="telemetry", message=message)
        new_time = time.perf_counter()
        print(new_time - old_time)
