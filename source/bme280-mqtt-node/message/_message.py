from abc import ABC, abstractmethod

from message_metadata import IMessageMetadata
from message_payload import IMessagePayload


class IMessage(ABC):

    @property
    @abstractmethod
    def metadata(self) -> IMessageMetadata:
        """IMessageMetadata."""

    @property
    @abstractmethod
    def payload(self) -> IMessagePayload:
        """IMessagePayload."""


class Message(IMessage):
    _metadata: IMessageMetadata
    _payload: IMessagePayload

    def __init__(self, metadata: IMessageMetadata, payload: IMessagePayload):
        self._metadata = metadata
        self._payload = payload

    @property
    def metadata(self) -> IMessageMetadata:
        return self._metadata

    @property
    def payload(self) -> IMessagePayload:
        return self._payload
