from abc import ABC, abstractmethod


class IMessagePayload(ABC):

    @property
    @abstractmethod
    def temperature(self) -> float:
        """Temperature / °C"""

    @property
    @abstractmethod
    def humidity(self) -> float:
        """Humidity / %"""

    @property
    @abstractmethod
    def pressure(self) -> float:
        """Pressure / hPa."""


class MessagePayload(IMessagePayload):
    _temperature: float
    _humidity: float
    _pressure: float

    def __init__(self, temperature: float, humidity: float, pressure: float):
        self._temperature = temperature
        self._humidity = humidity
        self._pressure = pressure

    @property
    def temperature(self) -> float:
        return self._temperature

    @property
    def humidity(self) -> float:
        return self._humidity

    @property
    def pressure(self) -> float:
        return self._pressure
