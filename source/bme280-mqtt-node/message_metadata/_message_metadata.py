import datetime
from abc import ABC, abstractmethod


class IMessageMetadata(ABC):
    @property
    @abstractmethod
    def asset_guid(self) -> str:
        """Asset GUID."""

    @property
    @abstractmethod
    def asset_type_id(self) -> str:
        """Asset type id."""

    @property
    @abstractmethod
    def timestamp(self) -> datetime.datetime:
        """Timestamp of message."""


class MessageMetadata(IMessageMetadata):
    _asset_guid: str
    _asset_type_id: str
    _timestamp: datetime.datetime

    def __init__(self, asset_guid: str, asset_type_id: str, timestamp: datetime.datetime):
        self._asset_guid = asset_guid
        self._asset_type_id = asset_type_id
        self._timestamp = timestamp

    @property
    def asset_guid(self) -> str:
        return self._asset_guid

    @property
    def asset_type_id(self) -> str:
        return self._asset_type_id

    @property
    def timestamp(self) -> datetime.datetime:
        return self._timestamp
