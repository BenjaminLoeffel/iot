from abc import ABC, abstractmethod
from json import dumps

import paho.mqtt.client as mqtt

from message import IMessage
from message_metadata import IMessageMetadata
from message_payload import IMessagePayload


class IMqttService(ABC):

    @abstractmethod
    def publish(self, topic: str, message: IMessage) -> bool:
        """Publishes IMessage to topic, return True if successful"""


class Serializer:

    @staticmethod
    def message_metadata(message_metadata: IMessageMetadata) -> dict:
        return {"assetGuid": message_metadata.asset_guid,
                "assetTypeId": message_metadata.asset_type_id,
                "timestamp": message_metadata.timestamp.isoformat()}

    @staticmethod
    def message_payload(message_payload: IMessagePayload) -> dict:
        return {"temperature": message_payload.temperature,
                "humidity": message_payload.humidity,
                "pressure": message_payload.pressure}

    @staticmethod
    def message(message: IMessage) -> dict:
        return {"metadata": Serializer.message_metadata(message.metadata),
                "payload": Serializer.message_payload(message.payload)}


class MqttService(IMqttService):
    _mqtt_client: mqtt.Client

    def __init__(self, mqtt_client: mqtt.Client):
        self._mqtt_client = mqtt_client

    def publish(self, topic: str, message: IMessage) -> bool:
        payload = Serializer.message(message)
        result = self._mqtt_client.publish(topic=topic, payload=dumps(payload))
        return result
