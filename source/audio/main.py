import sounddevice as sd

if __name__ == "__main__":
    print(sd.query_devices())
    print(sd.query_devices(kind="input"))

    f_sampling = 96000
    duration = 10.0

    sd.default.device = "Focusrite USB ASIO, ASIO"
    sd.default.samplerate = f_sampling
    sd.default.channels = 2

    values = sd.rec(int(duration * f_sampling))
    sd.wait()
    a = 1
