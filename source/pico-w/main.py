import network
import usocket as socket
import machine
import max31865

# Connect to WiFi
def connect_to_wifi(ssid, password):
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print("Connecting to WiFi...")
        wlan.connect(ssid, password)
        while not wlan.isconnected():
            pass
    print("Connected to WiFi with IP address:", wlan.ifconfig()[0])

# REST API endpoint to return temperature
def get_temperature(s):
    s.send("HTTP/1.0 200 OK\r\n")
    s.send("Content-Type: application/json\r\n\r\n")
    temperature = spi.read_temp()
    response = '{"temperature": ' + str(temperature) + '}'
    s.send(response)
    s.close()

# Initialize SPI and MAX31865 sensor
spi = machine.SPI(miso=machine.Pin(22), mosi=machine.Pin(25), sck=machine.Pin(23))
sensor = max31865.MAX31865(spi)

# Connect to WiFi
connect_to_wifi("your_ssid", "your_password")

# Start REST API server
addr = socket.getaddrinfo("0.0.0.0", 8080)[0][-1]
s = socket.socket()
s.bind(addr)
s.listen(1)
print("Listening on port 8080...")

while True:
    cl, addr = s.accept()
    print("Client connected from:", addr)
    request = cl.recv(1024)
    request = str(request, 'utf-8')
    if request.startswith("GET /temperature"):
        get_temperature(cl)