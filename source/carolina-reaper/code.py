
# SPDX-FileCopyrightText: Copyright (c) 2022 Erik Hess
#
# SPDX-License-Identifier: MIT

import board
from digitalio import DigitalInOut
from hx711.hx711_gpio import HX711_GPIO
import wifi
import socketpool
import socketpool
import secrets
import adafruit_minimqtt.adafruit_minimqtt as MQTT
import time
from interpolator import LinearInterpolator
from ulab import numpy as np
# Connect to WIFI
print(f"Connecting to {secrets.WIFI_SSID}")
wifi.radio.connect(secrets.WIFI_SSID, secrets.WIFI_PASSWORD)
print(f"Connected to {secrets.WIFI_SSID}")

# Create a socket pool
pool = socketpool.SocketPool(wifi.radio)

# Set up a MiniMQTT Client
mqtt_client = MQTT.MQTT(broker=secrets.MQTT_BROKER, port=secrets.MQTT_PORT, socket_pool=pool)

print(f"Attempting to connect to {mqtt_client.broker}")
mqtt_client.connect()

gpio_data = DigitalInOut(board.GP20)
gpio_clk = DigitalInOut(board.GP21)

hx = HX711_GPIO(gpio_data, gpio_clk, tare=True)

mqtt_topic = "carolina-reaper/raw"

values=np.zeros(10)
interpolator=LinearInterpolator(p1=(-21349.98916,0),p2=(868754.2518,19.9))
while True:

    for index in range(10):
        reading_raw = hx.read_raw()
        values[index]=reading_raw
    median=np.median(values)
    weight=interpolator.interpolate(median)
    print(median,weight)
    mqtt_client.publish("carolina-reaper/raw", median)
    mqtt_client.publish("carolina-reaper/weight", weight)
    time.sleep(0.5)